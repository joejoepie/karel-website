from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

class UsernameOrEmailBackend(ModelBackend):
    def authenticate(self, request, username=None, email=None, password=None, **kwargs):
        UserModel = get_user_model()
        user = None
        try:
            user = UserModel.objects.get(username=username)
        except:
            try:
                user = UserModel.objects.get(email=email)
            except:
                return
        
        if user is not None and user.check_password(password):
            return user
        else:
            return None
