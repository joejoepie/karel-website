
class Hond:
    def __init__(self, name, bite_power):
        self.name = name
        self.bite_power = bite_power
        self.defence = 1
    def bijt(self):
        print("IK BIJT met {} kracht".format(self.bite_power))
    def get_defence(self):
        return self.defence

class Labrador(Hond):
    def __init__(self, name, bite_power):
        super().__init__(name, bite_power)


    def lik(self):
        print("{} heeft je gelikt met veel liefde!".format(self.name))

kasper = Hond("Kasper", 69)
kasper.bijt()
kasper.defence = 5
print(kasper.get_defence())

lars = Labrador("Lars", 50)
lars.lik()