from django.urls import path
from . import views
from . import api

urlpatterns = [
    path('', views.index, name='index'),
    path('login', api.login, name='index'),
    path('register', api.register, name='register'),
    path('upload', views.upload_photo, name='upload'),
    path('logout', api.logout, name="logout"),
    path('user/<name>', views.user, name="user"),
]