# Generated by Django 2.2.5 on 2019-09-16 22:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0004_auto_20190916_2359'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='upload_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
