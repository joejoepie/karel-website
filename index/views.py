from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.conf import settings
import uuid
import os
from .forms import RegisterForm, UploadForm
from .models import Image

# Create your views here.
def index(req):
    names = [
        'karel',
        'wouter',
        'emiel',
        'steven',
        'arne'
    ]
    context = {
        'title': 'Geedde',
        'username': 'joejoepie',
        'names': names,
        'form': RegisterForm
    }
    return render(req, 'index.html', context)

@login_required
def upload_photo(req):
    if req.method == 'POST':
        form = UploadForm(req.POST, req.FILES)
        if form.is_valid():
            
            file = req.FILES['file']
            id = uuid.uuid4()
            _, extension = os.path.splitext(file.name)
            with open("index/static/{}{}".format(id, extension), 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            image = Image.objects.create(name='{}{}'.format(id, extension), file_size=69, user=req.user)
            return HttpResponse("Upload succesful!")
        return render(req, 'upload.html', {'form': form})
    else:
        return render(req, 'upload.html')
@login_required
def user(req, name):
    try:
        user = User.objects.get(username=name)
        pictures = Image.objects.filter(user=user)
        return render(req, 'user.html', {'images': pictures})
    except:
        return HttpResponse("User not found")