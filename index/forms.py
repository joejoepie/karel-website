from django.forms import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms.fields import CharField, FileField
from django.conf import settings


class LoginForm(forms.Form):
    usernameOrEmail = CharField()
    password = CharField()

class UploadForm(forms.Form):
    file = FileField()
    def clean_content(self):
        content = self.cleaned_data['file']
        content_type = content.content_type.split("/")[0]
        if content_type in settings.CONTENT_TYPES:
            if content.size > settings.MAX_UPLOAD_SIZE:
                raise forms.ValidationError("File too big")
        else:
            raise forms.ValidationError("File type not supported")
            
        return content

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')
    def save(self, commit=True):
        super().save(commit)