from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login, logout as django_logout
from django.contrib.auth.forms import UserCreationForm
from .forms import RegisterForm, LoginForm

def login(req):
    if req.method == "POST":
        form = LoginForm(req.POST)
        if form.is_valid():
            usernameOrEmail = form.cleaned_data['usernameOrEmail']
            password = form.cleaned_data['password']
            user = authenticate(req, username=usernameOrEmail, email=usernameOrEmail, password=password)
            if user is not None:
                django_login(req, user)
                return HttpResponse("Log in succesful")
            else:
                return HttpResponse("Failed to login!")
        else:
            render(req, 'index.html', {'form': form})

    return HttpResponse("Something went wrong")
def register(req):
    if req.method == 'POST':
        form = RegisterForm(req.POST)
        if form.is_valid():
            user = form.save()
            django_login(req, user)
            return HttpResponse('Account created!')
        else:
            return render(req, 'index.html', {'form': form})
    
def logout(req):
    django_logout(req)
    return HttpResponse("Logged out!")

