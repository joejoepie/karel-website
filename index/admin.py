from django.contrib import admin

# Register your models here.
from .models import Image

class ImageAdmin(admin.ModelAdmin):
    list_display = ['name', 'upload_date', 'user']


admin.site.register(Image, ImageAdmin)
