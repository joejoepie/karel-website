from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Image(models.Model):
    name = models.CharField(max_length=150, primary_key=True)
    upload_date = models.DateTimeField(auto_now_add=True)
    file_size = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    @classmethod
    def create(cls, name, file_size, user):
        image = cls(name=name, file_size=file_size, user=user)
        return image